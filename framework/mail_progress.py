import smtplib, ssl
from tqdm import tqdm

port = 465  # For SSL
me = "arjoonn.msccsc5@iiitmk.ac.in"
password = input("Password:>")

# Create a secure SSL context
context = ssl.create_default_context()
with open("../report.md", "r") as fl:
    people = [
        line.strip().split("|")
        for line in fl.readlines()
        if "|" in line and "-----" not in line
    ]
    fl.seek(0)
    generated_on = [line.strip() for line in fl.readlines() if "Generated on" in line][
        0
    ]

with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
    server.login(me, password)
    # ============
    for _, name, hr, assign, email, hrlink, _ in tqdm(people):
        if "@" not in email:
            continue
        to = email.strip()
        name = name.strip()
        marks_obtained = int(hr) + int(assign)
        total = 40
        expected_marks = 5
        message = f"""Subject: CDA2308 Automated Reminder\n\n
        Hi {name},

        This is an automated reminder for your course.
        Based on the report {generated_on}, you have:
        """
        if "https" not in hrlink.strip():
            message += """
        - not added your hackerrank link to the repo yet.
          Since that's an easy way to get marks in this course so please do that
          whenever you can.
        """
        message += f"""
        - current internals at {marks_obtained}/{total}

        If there is anything that you are finding difficult/confusing, please open issues on gitlab regarding
        them and I will help in whatever way I can.

        I'll look forward to your increased participation in the course.

        Sincerely,
        Arjoonn Sharma
        """
        server.sendmail(me, to, message)
